package main

import (
	"encoding/json"
	"time"
	"gitlab.com/flaneurtv/samm-client/samm"
	"sync"
	"fmt"
)

var i int = 0
var testrun = 0
var testmu sync.Mutex

// TickPayload is a payload and response on tick message
type BenchmarkTrigger struct {
	Iterations int `json:"iterations"`
}

type BenchmarkPing struct {
	BenchmarkRun int `json:"run"`
	StartNanoseconds int64 `json:"start_ns"`
	Iterations int `json:"iterations"`
	PingIndex     int `json:"index"`
}

type BenchmarkResult struct {
	BenchmarkRun int `json:"run"`
	Iterations     int `json:"interations"`
	Duration     int64 `json:"duration"`
	OpsPerSec     float64 `json:"ops_per_sec"`
	DurPerOp     string `json:"duration_per_op"`
}

type Tick struct {
	TickUUID      string `json:"tick_uuid"`
	TickTimestamp string `json:"tick_timestamp"`
}

func syncHandlePing(c *samm.Client, msg *samm.Message) {
	ping := &BenchmarkPing{}
	err := json.Unmarshal([]byte(*msg.Payload), ping)
	if err != nil {
		c.WriteErrf("Can't marshal response object %s", err)
		return
	}
/*
	payload := &BenchmarkPing{
		BenchmarkRun: ping.BenchmarkRun,
		StartNanoseconds: ping.StartNanoseconds,
		Iterations: ping.Iterations,
		PingIndex: ping.PingIndex,
	}

	m := c.NewMessage("pong", payload, "BenchmarkPing")
	c.EmitMessage(m)
*/
	if ping.PingIndex == ping.Iterations - 1 {
		stop := time.Now()
		duration := stop.Sub(time.Unix(0, ping.StartNanoseconds))
		result := &BenchmarkResult{
			BenchmarkRun: ping.BenchmarkRun,
			Iterations: ping.Iterations,
			Duration: int64(duration),
			OpsPerSec: float64(ping.Iterations)/duration.Seconds(),
			DurPerOp: fmt.Sprintf("%v",time.Duration(duration.Nanoseconds()/int64(ping.Iterations))),
		}
		m := c.NewMessage("result", result, "BenchmarkResult")
		c.EmitMessage(m)
	}
}

func asyncHandleTrigger(c *samm.Client, msg *samm.Message) {
	trg := &BenchmarkTrigger{}
	err := json.Unmarshal([]byte(*msg.Payload), trg)
	if err != nil {
		c.WriteErrf("Can't marshal response object %s", err)
		return
	}
  emitPings(c, trg.Iterations)
}

func asyncHandleTick(c *samm.Client, msg *samm.Message) {
	emitPings(c, 1000)
}

func emitPings(c *samm.Client, iterations int) {
	testmu.Lock()
	testrun++
	t := testrun
	testmu.Unlock()

	start := time.Now()

	for j := 0; j < iterations; j++ {
		payload := &BenchmarkPing{
			BenchmarkRun: t,
			StartNanoseconds: start.UnixNano(),
			Iterations: iterations,
			PingIndex: j,
		}
		m := c.NewMessage("ping", payload, "BenchmarkPing")
		c.EmitMessage(m)
	}

	stop := time.Now()
	duration := stop.Sub(start)
	result := &BenchmarkResult{
		BenchmarkRun: t,
		Iterations: iterations,
		Duration: int64(duration),
		OpsPerSec: float64(iterations)/duration.Seconds(),
		DurPerOp: fmt.Sprintf("%v",time.Duration(duration.Nanoseconds()/int64(iterations))),
	}
	m := c.NewMessage("result_pub", result, "BenchmarkResult")
	c.EmitMessage(m)

}

func main() {
	c := samm.NewClient()
	c.RegisterTopic("ping", syncHandlePing)
	c.RegisterTopic("trigger", asyncHandleTrigger)
	c.RegisterTopic("tick", asyncHandleTick)
	c.Listen()
}
