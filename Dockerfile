# -- Builder Container -- #
FROM golang:1.10 AS builder

WORKDIR /go/src/gitlab.com/flaneurtv/samm-client
#RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

COPY ./src/. /go/src/gitlab.com/flaneurtv/samm-client/.
#RUN dep ensure

RUN cd /go/src/gitlab.com/flaneurtv/samm-client && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
    -o /srv/processor ./main.go

# -- Runtime Container -- #
# FROM alpine:3.8

ENV SERVICE_NAME=client
WORKDIR /srv/

COPY --from=flaneurtv/samm /usr/local/bin/samm /usr/local/bin/samm
COPY ./subscriptions.txt /srv/subscriptions.txt

COPY ./processor.sh /srv/processor.sh
ENV SERVICE_PROCESSOR=/srv/processor.sh

CMD ["samm"]
